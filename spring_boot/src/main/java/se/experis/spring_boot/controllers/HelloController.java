package se.experis.spring_boot.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/*
*   CONTROLLER
*
*   Controller for GET-parameters sent to "/hello".
*   The program always expects a name but "greeting" and "reverse" is optional.
*/
@RestController
public class HelloController {

    /*
    *   Method for reading GET-parameters.
    *   If parameter "greeting" is present, return a greeting.
    *   If parameter "reverse" is present, return the name in reverse.
    *   Else return an error message.
    */
    @RequestMapping(value = "/GreetingOrReverse", method = GET)
    public String greetGuest(@RequestParam("name") String name,
                             @RequestParam("greeting") Optional<String> greeting,
                             @RequestParam("reverse") Optional<String> reverse) {

        List<Character> nameList;
        nameList = new ArrayList<>();

        StringBuilder reverseName = new StringBuilder();

        if (greeting.isPresent()) {

            return "Hey " + name + "!";

        }

        else if (reverse.isPresent()) {

            for (int i = 0; i < name.length(); i++) {

                nameList.add(name.charAt(i));

            }

            // Reversing nameList
            Collections.reverse(nameList);

            for (Character character : nameList) {

                reverseName.append(character);

            }

            return reverseName.toString();
        }

        return "Something went wrong";

    }

}