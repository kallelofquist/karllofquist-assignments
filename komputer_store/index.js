/*
*	By: Karl Löfquist
*/

let loanTakenFlag = false;
const laptopArray = [];

/*
*	Calling method to create Computer objects
*/
createLaptops();

/*
*	Function that runs when the entire index.html has been loaded
*/
function init() {

	// Initiating interactive elements
	const takeLoanButton = document.getElementById('takeLoan');
	const bankButton = document.getElementById('bank');
	const workButton = document.getElementById('work');
	const laptopDropdown = document.getElementById('laptopSelect');
	const buyButton = document.getElementById('buy');

	// Initiating economic elements
	const loanAmountElem = document.getElementById('loanAmount');
	const payElem = document.getElementById('pay');
	const balanceElem = document.getElementById('balance');

	// Initiating alert element
	const laptopBoughtAlert = document.getElementById('laptopBoughtAlert');

	// Initiating elements related to computer information
	const laptopDisplayElem = document.getElementById('displayLaptop');
	const laptopShortInfoElem = document.getElementById('laptopShortInfo');
	const laptopTitleElem = document.getElementById('laptopTitle');
	const laptopImageElem = document.getElementById('laptopImage');
	const laptopLongInfoElem = document.getElementById('laptopLongInfo');
	const laptopPriceElem = document.getElementById('laptopPrice');

	// Adding event listeners to interactive elements
	takeLoanButton.addEventListener('click', () => getLoan(balanceElem, loanAmountElem));
	bankButton.addEventListener('click', () => bankSalary(payElem, balanceElem));
	workButton.addEventListener('click', () => getPaid(payElem));
	laptopDropdown.addEventListener('change', () => newLaptopSelected(laptopDropdown, laptopDisplayElem, laptopBoughtAlert,
																	  laptopShortInfoElem, laptopTitleElem, laptopImageElem,
																	  laptopLongInfoElem, laptopPriceElem));
	buyButton.addEventListener('click', () => buyLaptop(laptopPriceElem, balanceElem, laptopDisplayElem,laptopBoughtAlert));

}

// Adding event listener to window object to call the init() function when the index page has been loaded
window.addEventListener('load', init);

/*
*	Function that grants the user a loan. Or not. Called when klicking the "Take Loan"-button in the bootstrap modal
*/
function getLoan(balanceElem, loanAmountElem) {

	let balance = parseInt(balanceElem.innerHTML);
	let loanAmount = parseInt(loanAmountElem.value);

	// If the requested loan is less or equal to the bank balance and the user has not taken a loan before...
	if (loanAmount <= (balance * 2) && !loanTakenFlag) {
		// ... The user gets a loan and the loanTakenFlag is set to true.
		balanceElem.innerHTML = (balance + loanAmount) + " kr";
		loanTakenFlag = true;
	}

	else if (loanTakenFlag) {
		alert("You can't take anymore loans until you have bought a computer!");
	}

	else {
		alert("You can't take a loan that is higher than twice the amount of your balance!");
	}

}

/*
*	Function that banks the users sallary (pay). Called when klicking the "Bank"-button. 
*/
function bankSalary(payElem, balanceElem) {

	let pay = parseInt(payElem.innerHTML);
	let balance = parseInt(balanceElem.innerHTML);

	// Updates the bank balance
	balanceElem.innerHTML = (pay + balance) + " kr";

	// Empties the users pay
	payElem.innerHTML = "0 kr";
}

/*
*	Function that pays the user when working. Called when klicking the "Work"-button.
*/
function getPaid(payElem) {

	let pay = parseInt(payElem.innerHTML);

	pay = (pay + 100);

	// Updates the users pay
	payElem.innerHTML = pay + " kr";

}

/*
*	Function that displays relevant laptop information. Called when the user selects a new laptop from dropdown.
*/
function newLaptopSelected(laptopDropdown, laptopDisplayElem, laptopBoughtAlert,
						   laptopShortInfoElem, laptopTitleElem, laptopImageElem, 
						   laptopLongInfoElem, laptopPriceElem) {

	// Sets the alert for when a user has bought a computer to invisible
	laptopBoughtAlert.style.display = "none";

	// If the user has selected a valid computer from the dropdown...
	if (parseInt(laptopDropdown.value) >= 0) {

		// ... Show the block for computer information
		laptopDisplayElem.style.display = "block";

		// Set chosen computer to correspond with the right one in the laptopArray[].
		let computer = laptopArray[laptopDropdown.value];

		// Update elements in the block for computer information
		laptopShortInfoElem.innerHTML = computer.shortDescription;
		laptopTitleElem.innerHTML = computer.title;
		laptopImageElem.src = computer.imageURL;
		laptopLongInfoElem.innerHTML = computer.longDescription;
		laptopPriceElem.innerHTML = computer.price + " kr";

	}

	else {

		// Hide block for computer information
		laptopDisplayElem.style.display = "none";

		// Update elements in the block for computer information
		laptopShortInfoElem.innerHTML = "No info to show :(";
		laptopTitleElem.innerHTML = "";
		laptopImageElem.src = "";
		laptopLongInfoElem.innerHTML = "";
		laptopPriceElem.innerHTML = "";
	}

}

/*
*	Function that buys a computer for the user. Called when klicking the "Buy Laptop"-button.
*/
function buyLaptop(laptopPriceElem, balanceElem, laptopDisplayElem, laptopBoughtAlert) {

	// If the users bank balance is greater than or equal to the current laptop price...
	if (parseInt(balanceElem.innerHTML) >= parseInt(laptopPriceElem.innerHTML)) {

		// Set loanTakenFlag to false so the user can take a loan again
		loanTakenFlag = false;
		
		let balance = parseInt(balanceElem.innerHTML);
		let price = parseInt(laptopPriceElem.innerHTML);

		// Updates the bank balance
		balanceElem.innerHTML = (balance - price) + " kr";

		// Hide block for computer information and show alert for bought a computer
		laptopDisplayElem.style.display = "none";
		laptopBoughtAlert.style.display = "block";

	}

	else{
		alert("This laptop is too expensive!");
	}

}

/*
*	Function that creates laptops and puts them in the laptopArray[]. This function is called when this script file is executed.
*/
function createLaptops() {

	laptopArray.push(new Computer(1, "Dall", "This is a pretty bad computer", "images/dall.jpg",
		"This is a cheap laptop with a pretty bad core. You can use it as a calculator. Maybe...", 500));

	laptopArray.push(new Computer(2, "Lenove", "This is a computer that is a bit better than the Dall", "images/lenove.jpg",
		"This is a relatively cheap laptop with a pretty OK core. You can use it to surf the web. Like Facebook and stuff.", 1000));

	laptopArray.push(new Computer(3, "MecMagazine Pro", "This is a computer that is actually pretty good", "images/mecmagazine.jpg",
		"This is a pretty expensive laptop with a good core. You can use it to surf the web, play some games and develop apps.", 5000));

	laptopArray.push(new Computer(4, "Acir", "This is a really hardcore computer", "images/acir.jpg",
		"This is an expensive laptop with an amazing core. Really a beast of a computer! You can use it to surf the web, play virtually all games and develop 4000 apps at the same time.", 10000));

}