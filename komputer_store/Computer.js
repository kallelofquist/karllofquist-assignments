/*
*	By: Karl Löfquist
*/

class Computer {
  constructor(id, title, shortDescription, imageURL, longDescription, price) {
    this.id = id;
    this.title = title;
    this.shortDescription = shortDescription;
    this.imageURL = imageURL;
    this.longDescription = longDescription;
    this.price = price;
  }
}