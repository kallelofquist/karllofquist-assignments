package se.experis;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/*
*   Program for searching for a string (from user input) in information stored in JSON-objects.
*/

public class Program {

    private static String userInput;

    final static String PRE = "\u001b[31m";
    final static String PRO = "\u001b[0m";

    public static void main(String[] args) {

        System.out.println("It works so far!");

        readInput();
        readJSONFile();
    }

    /*
    *   Getter and setter.
    */

    public static String getUserInput() {
        return userInput;
    }

    public static void setUserInput(String userInput) {
        Program.userInput = userInput;
    }

    /*
    *   Method for reading user input.
    */

    private static void readInput() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Search for name or phone number: ");

        try {

            setUserInput(reader.readLine());

        }

        catch (IOException e) {

            e.printStackTrace();

        }

    }

    /*
    *   Method for reading a JSON-file. When a file has been read, each JSON-object in the JSON-Array
    *   is sent to the method searchInPersonObject().
    */

    private static void readJSONFile() {

        //System.out.println(getUserInput());

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try(InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("People.JSON")){

            if (in == null) {
                throw new AssertionError();
            }

            JSONArray jsonArray = (JSONArray) jsonParser.parse(new InputStreamReader(in));
            jsonArray.forEach( person -> searchInPersonObject( (JSONObject) person ));

        }
        catch(Exception e){
            throw new RuntimeException(e);
        }

    }

    /*
    *   Method for searching in the JSON person-object. The method searches in first- and last name and the two
    *   different phone numbers for the users input. If there is a hit, the persons information is printed out.
    */

    private static void searchInPersonObject(JSONObject person) {

        //Get person first name
        String firstName = (String) person.get("firstName");

        //Get person last name
        String lastName = (String) person.get("lastName");

        //Get person mobile number
        String mobileNumber = (String) person.get("mobileNumber");

        //Get person work number
        String workNumber = (String) person.get("workNumber");

        StringBuilder outputInfo = new StringBuilder(firstName + " " + lastName + ". | Mobile: " + mobileNumber + " Work: " + workNumber
                + " | Date of birth: " + person.get("dateOfBirth"));

        if (firstName.toLowerCase().contains(getUserInput().toLowerCase())) {

            System.out.println(outputInfo.toString().toLowerCase().replace(getUserInput().toLowerCase(), PRE + getUserInput() + PRO));

        }

        else if (lastName.toLowerCase().contains(getUserInput().toLowerCase())) {

            System.out.println(outputInfo);

        }

        if (mobileNumber.contains(getUserInput())) {

            System.out.println(outputInfo);

        }

        else if (workNumber != null && workNumber.contains(getUserInput())) {

            System.out.println(outputInfo);

        }

    }
}
