package se.experis;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/*
*   Initiator. This class checks for an input (int) in args[0] and sets this to userInput. If the user has specified a
*   valid value, information about a customer in Chinook_Sqlite.sqlite with corresponding ID will be printed out.
*/
public class Program {

    // Setup
    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection conn = null;

    public static void main(String[] args) {
        int userInput;

        if (args.length == 1) {
            userInput = Integer.parseInt(args[0]);
        }
        else {
            userInput = 0;
        }

        printCustomerNameFromDatabase(userInput);
    }

    /*
    *   Method for getting all customers from the database to be set in array customers. Then a specified customer is
    *   printed out based on user input (or random, if there is no user input).
    */
    private static void printCustomerNameFromDatabase(int customerId) {

        List<Customer> customers = new ArrayList<>();

        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite OK.");

            // Set SQL-query
            String allCustomersSql = "SELECT CustomerId,FirstName,LastName,City,Phone FROM Customer;";

            // Prepare Statement
            PreparedStatement preparedCustomersStatement = conn.prepareStatement(allCustomersSql);

            // Execute Statement
            ResultSet resultSet = preparedCustomersStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("City"),
                                resultSet.getString("Phone")
                        )
                );
            }
        }
        catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        if (customerId == 0) {
            Random rand = new Random();
            customerId = rand.nextInt(customers.size() + 1);
        }

        for (Customer customer : customers) {

            if (Integer.parseInt(customer.getCustomerID()) == (customerId)) {

                System.out.println(customer.getFirstName() + " " + customer.getLastName());
                printCustomersPopularGenre(customer);

            }
        }
    }

    /*
    *   Method for getting the specified customer's most popular genre. It then prints out the result.
    */
    private static void printCustomersPopularGenre(Customer customer) {

        String mostPopularGenre = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite OK.");

            // Set SQL-query
            String updatePositionSql = "SELECT Name FROM Genre WHERE GenreId IN" +
                    "(SELECT GenreId FROM Track WHERE TrackID IN" +
                    "(SELECT TrackID FROM InvoiceLine WHERE InvoiceId IN" +
                    "(SELECT InvoiceId FROM Invoice WHERE CustomerID=?))" +
                    " GROUP BY GenreID ORDER BY COUNT (GenreId) DESC LIMIT 1);";

            // Prepare Statement
            PreparedStatement preparedStatement = conn.prepareStatement(updatePositionSql);
            preparedStatement.setString(1, String.valueOf(customer.getCustomerID()));

            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                mostPopularGenre = resultSet.getString("Name");
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }

        System.out.println(Objects.requireNonNullElse(mostPopularGenre, "Could not find most popular genre"));
    }

}
