package se.experis;

public class Customer {

    private String customerID;
    private String firstName;
    private String lastName;
    private String city;
    private String phone;

    /*
    *   Class for saving data that is associated with a customer in the Chinook_Sqllite.sqlite database.
    */
    public Customer(String customerID, String firstName, String lastName, String city, String phone) {

        setCustomerID(customerID);
        setFirstName(firstName);
        setLastName(lastName);
        setCity(city);
        setPhone(phone);

    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
