package se.experis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PetRockTest {

    private PetRock petRock;

    @BeforeEach
    void setUp() {
        petRock = new PetRock("Oskar");
    }

    @Test
    void getName() {
        assertEquals("Oskar", petRock.getName());
    }

    @Test
    void testUnhappyToStart() {
        assertFalse(petRock.isHappy());
    }

    @Test
    void testHappyAfterPlay() {
        petRock.playWithRock();
        assertTrue(petRock.isHappy());
    }

    @Test
    void throwingException() {
        assertThrows(IllegalStateException.class, () -> petRock.getHappyMessage());
    }

    @Test
    void avoidException() {
        petRock.playWithRock();
        assertEquals("I'm happy!", petRock.getHappyMessage());
    }

    @Test
    void testFavNumber() {
        assertEquals(42, petRock.getFavNumber());
    }

    @Test
    void emptyNameFail() {
        assertThrows(IllegalStateException.class, () -> new PetRock(""));
    }
}
