package se.experis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/*
*   Class for handling events related to searching for and printing out information regarding characters.
*   The character is selected by the user by entering an ID.
*/
public class CharacterHandler {

    private final String userInput;
    private final StringBuilder urlToCharacterPath;

    public CharacterHandler(String input, StringBuilder urlToCharacter) {

        userInput = input;
        urlToCharacterPath = urlToCharacter;

    }

    /*
    *   Method for finding a character based on an ID and displaying the entire JSON object.
    */
    public JSONArray getCharacter() {

        if (Integer.parseInt(userInput) > 0) {
            urlToCharacterPath.append(userInput);
        } else {
            System.out.println("Not a valid number");
            return null;
        }

        try {
            URL url = new URL(urlToCharacterPath.toString());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                // print content formatted as JSON
                System.out.println(" ---  Found Character --- ");
                JSONObject json = new JSONObject(content.toString());
                System.out.println(json.toString(4));
                System.out.println(" --------------------- \n");

                return json.getJSONArray("allegiances");

            } else {
                System.out.println("Error");
                System.out.println("Server responded with: " + con.getResponseCode());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return null;
    }

}
