package se.experis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/*
*   Class for handling (finding and printing) P.O.V characters in books published by Bantam Books.
*/
public class PovCharactersHandler {

    private String nameOfPublisher;
    private StringBuilder urlToBooksPath = new StringBuilder();
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";

    public PovCharactersHandler(String publisher, StringBuilder booksPath) {

        nameOfPublisher = publisher;
        urlToBooksPath.append(booksPath);

    }

    /*
    *   Method for finding each book published by bantam books and putting each found JSON object into a list.
    */
    public void checkForBantamBooks() {

        List<JSONObject> bantamBooks = new ArrayList<>();

        try {
            URL url = new URL(urlToBooksPath.toString());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                JSONArray jsonArray = new JSONArray(content.toString());

                for (Object jsonObject : jsonArray) {

                    JSONObject jsonObj = (JSONObject) jsonObject;

                    if (jsonObj.getString("publisher").equals(nameOfPublisher)) {
                        bantamBooks.add(jsonObj);
                    }

                }


            } else {
                System.out.println("Error");
                System.out.println("Server responded with: " + con.getResponseCode());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        getPovCharacters(bantamBooks);

    }

    /*
    *   Method that goes through each JSON object (Bantam Book) and then each P.O.V character in those books.
    *   It then prints the title of the book in question and each P.O.V character in that book into a little bok.
    */
    public void getPovCharacters(List<JSONObject> bantamBooksList) {

        String leftAlignFormat = "| %-20s |%n";
        String TitleAlignFormat = "| %-32s |%n";
        String nameAlignFormat = "| %-23s |%n";

        for (Object bantamBook : bantamBooksList) {

            JSONObject bantamBookObject = (JSONObject) bantamBook;

            System.out.format(leftAlignFormat, "+---------------------+");
            System.out.format(TitleAlignFormat, ANSI_BLUE + bantamBookObject.getString("name") + ANSI_RESET);
            System.out.format(leftAlignFormat, "+---------------------+");

            JSONArray characterArray = null;

            if (!bantamBookObject.getJSONArray("povCharacters").isEmpty()) {
                characterArray = bantamBookObject.getJSONArray("povCharacters");
            }
            else {
                System.out.format(nameAlignFormat, "*no P.O.V characters*");
            }

            if (characterArray != null) {

                for (Object characterUrl : characterArray) {

                try {
                    URL url = new URL((String) characterUrl);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");

                    if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                        InputStreamReader isr = new InputStreamReader(con.getInputStream());
                        BufferedReader br = new BufferedReader(isr);
                        String inputLine;
                        StringBuilder content = new StringBuilder();

                        while ((inputLine = br.readLine()) != null) {
                            content.append(inputLine);
                        }
                        br.close();

                        JSONObject json = new JSONObject(content.toString());
                        System.out.format(nameAlignFormat, json.getString("name"));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                }

            }

        }

    }
}
