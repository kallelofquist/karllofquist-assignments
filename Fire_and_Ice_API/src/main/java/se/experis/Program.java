package se.experis;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
*   Initiation class for starting the program. Reads the users initial input.
*/

public class Program {

    private static final StringBuilder urlToCharacterPath = new StringBuilder();
    private static final StringBuilder urlToBooksPath = new StringBuilder();

    public static void main(String[] args) {

        urlToCharacterPath.append("https://anapioficeandfire.com/api/characters/");
        urlToBooksPath.append("https://anapioficeandfire.com/api/books/");


        String userInput = getUserInput();
        CharacterHandler characterHandler = new CharacterHandler(userInput, urlToCharacterPath);
        JSONArray urlToHouses = characterHandler.getCharacter();

        HouseHandler houseHandler = new HouseHandler(urlToHouses);
        houseHandler.checkForAffiliateHouses();

        PovCharactersHandler povCharactersHandler = new PovCharactersHandler("Bantam Books", urlToBooksPath);
        povCharactersHandler.checkForBantamBooks();
    }

    /*
    *   Method for getting input from user in the form of an ID for a character.
    */
    private static String getUserInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter id of character (number): ");

        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
