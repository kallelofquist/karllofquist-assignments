package se.experis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/*
*   Class for handling the earlier specified characters house-allegiances. Displays the house(s) and each of the house
*   in questions allegiances.
*/
public class HouseHandler {

    private final JSONArray arrayOfHouses;

    public HouseHandler(JSONArray houseArray) {

        arrayOfHouses = houseArray;

    }

    /*
    *   Method for checking for affiliate houses to the earlier specified character. The method prompts the user to type
    *   y (yes) or n (no) to choose if he/she wants to display sworn members to the characters house(s).
    */
    public void checkForAffiliateHouses() {

        if (arrayOfHouses == null) {
            return;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String userChoice = "n";

        if (!arrayOfHouses.isEmpty()) {
            System.out.println("Show sworn members of the characters house? If no, display each book with publisher Bantam Books and its P.O.V character (y/n): ");

            try {
                userChoice = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (Objects.equals(userChoice.toLowerCase(), "y")) {

                for (Object houses : arrayOfHouses) {

                    getMembersOfHouse((String) houses);
                }

            }

        }

        else {
            System.out.println("This character is not sworn to any house.");
        }

    }

    /*
    *   Method for getting the houses name and sworn members. Prints out the names when ALL of the names have been found.
    */
    public void getMembersOfHouse(String houses) {

        List<String> characterList = new ArrayList<>();

        try {
            URL url = new URL(houses);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                // print content formatted as JSON
                JSONObject json = new JSONObject(content.toString());
                System.out.println(" ---  Characters belonging to: " + json.getString("name") + " ---");

                System.out.print("\nloading");

                for (Object characters : json.getJSONArray("swornMembers")) {

                    try {
                        url = new URL(characters.toString());
                        con = (HttpURLConnection) url.openConnection();
                        con.setRequestMethod("GET");

                        if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                            isr = new InputStreamReader(con.getInputStream());
                            br = new BufferedReader(isr);
                            content = new StringBuilder();

                            while ((inputLine = br.readLine()) != null) {
                                content.append(inputLine);
                            }
                            br.close();

                            json = new JSONObject(content.toString());

                            if (!characterList.contains(json.getString("name"))) {
                                characterList.add(json.getString("name"));
                            }

                            System.out.print(".");
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n");

        for (String characterName : characterList) {

            System.out.println(characterName);

        }

        try {
            System.out.println("\nPress Enter to display each book published by Bantam Books and its P.O.V Characters");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            reader.readLine();
        }
        catch (IOException ex) {
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }


    }
}
