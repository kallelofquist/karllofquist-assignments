package se.experis;


/*
*   Class for handling the user input. It runs the numbers through a method that calculates a sum based on the luhn algorithm.
*   After this, a message is built based on the results from the algorithm.
*/
public class InputChecker {

    private final String input;
    public int expectedCheckDigit;
    public int checkDigit;
    public int luhnSum;

    /*
    *   Constructor.
    */
    public InputChecker(String in) {

        input = in;
        checkDigit = Integer.parseInt(input.substring(input.length() - 1));

    }

    /*
    *   Method for running the user input through the luhn algorithm. Returns true if the numbers follow the standard
    *   and false if they do not.
    */
    public boolean checkInput() {

        int nDigits = input.length();

        boolean isSecond = false;

        for (int i = nDigits - 1; i >= 0; i--) {

            int d = input.charAt(i) - '0';

            if (isSecond) {
                d = d * 2;
            }

            luhnSum += d / 10;
            luhnSum += d % 10;

            isSecond = !isSecond;

        }

        if (luhnSum % 10 == 0) {
            expectedCheckDigit = checkDigit;
            return true;
        }
        else {
            expectedCheckDigit = (10 - (luhnSum % 10) + checkDigit);
            if (expectedCheckDigit >= 10) {
                expectedCheckDigit = expectedCheckDigit - 10;
            }
            return false;
        }
    }

    /*
    *   A method that returns a result message based on how the previous method behaved.
    */
    public String printResults(boolean nSumDividableByTen) {
        StringBuilder output;

        output = new StringBuilder("\nInput: " + (input.substring(0, input.length() - 1)) + " " + checkDigit);
        output.append("\nProvided: ").append(checkDigit);
        output.append("\nExpected: ").append(expectedCheckDigit);

        output.append("\n\n").append("Checksum: ");

        if (nSumDividableByTen) {
            output.append("Valid");
        }
        else {
            output.append("Invalid");
        }

        output.append("\n").append("Digits: ").append(input.length());

        if (input.length() == 16) {
            output.append(" (credit card)");
        }

        return output.toString();
    }
}
