package se.experis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
*   Initializer for program. Includes method for prompting a user for input. The class the distributes the input
*   to InputChecker for validation. Prints out a message based on results.
*/
public class Program {

    public static void main(String[] args) {

        String userInput = getUserInput();

        InputChecker inputChecker = new InputChecker(userInput);
        System.out.println(inputChecker.printResults(inputChecker.checkInput()));

    }

    /*
    *   Method for reading user input.
    */
    public static String getUserInput() {

        try (BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in))){

            System.out.print("Please enter a credit card number: ");
            return inputReader.readLine();

        }

        catch (IOException ex){
            System.out.println("Something went wrong");
        }

        return null;

    }

}
