package se.experis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
*   Test class for the class InputChecker.
*/
class InputCheckerTest {

    public InputChecker inputCheckerCorrect;
    public InputChecker inputCheckerIncorrect;

    /*
    *   Initial setup.
    */
    @BeforeEach
    void setUp() {

        inputCheckerCorrect = new InputChecker("79927398713");
        inputCheckerIncorrect = new InputChecker("4112961738840532"); // Supposed to end with a 7

    }

    /*
    *   Method for checking the outcome of checkInput().
    */
    @Test
    void testInput() {

        assertTrue(inputCheckerCorrect.checkInput());

        assertFalse(inputCheckerIncorrect.checkInput());

    }

    /*
    *   Method for checking the expected check digit.
    */
    @Test
    void testExpectedCheckDigit() {
        inputCheckerCorrect.checkInput();
        assertEquals(inputCheckerCorrect.expectedCheckDigit, inputCheckerCorrect.checkDigit);

        inputCheckerIncorrect.checkInput();
        assertTrue(inputCheckerIncorrect.checkDigit != inputCheckerIncorrect.expectedCheckDigit);
    }

    /*
    *   Method for checking the actual user entered check digit.
    */
    @Test
    void testCheckDigit() {

        inputCheckerCorrect.checkInput();
        assertEquals(inputCheckerCorrect.checkDigit, (inputCheckerCorrect.luhnSum + inputCheckerCorrect.checkDigit) % 10);

        inputCheckerIncorrect.checkInput();
        assertNotEquals(inputCheckerIncorrect.checkDigit, (inputCheckerIncorrect.luhnSum + inputCheckerIncorrect.checkDigit) % 10);

    }

    /*
    *   Method for testing the result of the calculated luhn sum.
    */
    @Test
    void testLuhnSum() {

        inputCheckerCorrect.checkInput();
        assertEquals(0, inputCheckerCorrect.luhnSum % 10);

        inputCheckerIncorrect.checkInput();
        assertNotEquals(0, inputCheckerIncorrect.luhnSum % 10);

    }

    /*
    *   Method for checking the result message.
    */
    @Test
    void testOutput() {

        inputCheckerCorrect.checkInput();
        assertTrue(inputCheckerCorrect.printResults(true).contains("Valid"));

        inputCheckerIncorrect.checkInput();
        assertTrue(inputCheckerIncorrect.printResults(false).contains("Invalid"));

    }
}